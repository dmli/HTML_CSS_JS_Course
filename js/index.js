function addTime() {
    'use strict';

    var output = document.getElementById('time');
    var date = new Date();
    var offset = date.getTimezoneOffset(); //returns offset in minutes
    offset /= 60;
    var minutes;
    if (date.getUTCMinutes() < 10) {
        minutes = '0' + date.getUTCMinutes();
    }
    else {
        minutes = date.getUTCMinutes();
    }
    var seconds;
        if (date.getUTCSeconds() < 10) {
        seconds = '0' + date.getUTCSeconds();
    }
    else {
        seconds = date.getUTCSeconds();
    }
    output.innerHTML = (date.getUTCHours()-offset) + ':' + minutes + ':' + seconds;
}



function changeBackground() {
    'use strict'
    
    i++;
    var images = ['url("./images/bg.jpg")', 'url("./images/bg2.jpg")', 'url("./images/bg3.jpg")', 'url("./images/bg4.jpg")', 'url("./images/bg5.jpg")'];
    var html = document.getElementById('slideshow');
    html.style.backgroundImage = images[i];
    console.log(html.style.backgroundImage);
    
    
    if (i == images.length) {
        i = 0;
    }
}

function projects() {
    var projects = document.getElementById('current'); 
    console.log('Onload the background changes.');
    if (projects.href.substr(projects.href.length - 13) == 'projects.html') {
        changeBackground();
    }
}

function mouseOverAlarm() {
    console.log('Warning, you placed the mouse cursor over the first project!')
}
setInterval(addTime, 1000);
window.onload = addTime;
window.onload = projects;
setInterval(changeBackground, 25000);
var i = 0;