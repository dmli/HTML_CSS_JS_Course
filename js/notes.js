
var notes = new Array();

function addItem() {
	textbox = document.getElementById('item');
	var itemText = textbox.value;
	textbox.value = '';
	textbox.focus();
	var newItem = {title: itemText, quantity: 1};
	
	if (notes.length == 5) {
		notes.splice(0, 1);
	}
	
	notes.push(newItem);
	displayList()
}

function displayList() {
	var table = document.getElementById('list');
	table.innerHTML = '';
	
	for (var i = 0; i<notes.length; i++) {
		var node = undefined;
		var note = notes[i];
		var node = document.createElement('tr');
		var html = '<td>'+note.title+'</td>';
	    node.innerHTML = html;
		table.appendChild(node);
	}
}

function deleteIndex(i) {
	notes.splice(i, 1);
	displayList();
}

button = document.getElementById('add')
button.onclick = addItem;
