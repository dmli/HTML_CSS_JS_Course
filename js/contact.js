function contact() {

    var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var email = document.getElementById('email').value;
    var age = document.getElementById('age').value;
    var comment = document.getElementById('comment').value;
    var website = document.getElementById('website').value;
    
    var output = document.getElementById('output');
    
    output.innerHTML = firstName + ' ' + lastName + ', ' + age + ', ' + email + ', ' + website + ' left a comment saying: ' + comment;

    return false;
}

// Initial setup:
function init() {
    'use strict';
    document.getElementById('theForm').onsubmit = contact;
} // End of init() function.

window.onload = init;